
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

//Update Enemy Ship Iinformation
void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) // counts down when to spawn an enemy ship
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); //Reduces the delay value by the elapsed itme

		if (m_delaySeconds <= 0) // once the delay is less than or equal to 0 it spawns a ship
		{
			GameObject::Activate(); //spawns a ship
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); //counts how long ships have been active
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); //if they have been active longer than 2 seconds and are no longer on the screen, deactivate
	}

	Ship::Update(pGameTime); // updates the ship
}

//Create a new enemy ship
void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position); //assigns a position when spawning
	m_delaySeconds = delaySeconds; // assings the delay before spawning

	Ship::Initialize(); //creates a new enemy ship
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage); 
}